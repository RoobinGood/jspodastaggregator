define('js/request', 
	[], 
	function() {

	var get = function(url, successCallback) {
		// console.log("GET", url);
		$.get(url, successCallback);
	};

	return {
		get: get,
	};
});
