define('js/modelController', 
	['models/podster', 'models/devzen', 'models/radiovesti', 
	'models/kinopoisk'], 
	function(podster, devzen, radiovesti, kinopoisk) {

	var getModel = function(modelName) {
		return (modelName === "devzen") ? devzen :
			(modelName === "podster") ? podster :
			(modelName === "radiovesti") ? radiovesti :
			(modelName === "kinopoisk") ? kinopoisk :
			undefined;
	};

	return {
		getModel: getModel,
	};
});
