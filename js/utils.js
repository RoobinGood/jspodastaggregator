define('js/utils', 
	[], 
	function() {

	var deleteImages = function(data) {
		data.find("img").each(function(index) {
			this.src = "";
		});
		return data;
	};

	var getMonthAsNumber = function(str) {
		str = str.toLowerCase();
		return (str === "января") ? "01" :
			(str === "февраля") ? "02" :
			(str === "марта") ? "03" :
			(str === "апреля") ? "04" :
			(str === "мая") ? "05" :
			(str === "июня") ? "06" :
			(str === "июля") ? "07" :
			(str === "августа") ? "08" :
			(str === "сентября") ? "09" : 
			(str === "окятбря") ? "10" : 
			(str === "ноября") ? "11" : 
			(str === "декабря") ? "12" : 
			"хуябрь"
	};

	var fillZeros = function(n, width) {
		return n.length >= width ? n : 
			new Array(width - n.length + 1).join("0") + n;
	};

	var fixDash = function(str) {
		return str.replace("—", "-").replace("&#8212;", "-");
	};

	var fixSpace = function(str) {
		return str.replace(new RegExp(String.fromCharCode(160), "g"), " ");
	};

	return {
		deleteImages: deleteImages,
		getMonthAsNumber: getMonthAsNumber,
		fillZeros: fillZeros,
		fixDash: fixDash,
		fixSpace: fixSpace,
	};
});