require(['can', 'js/newEpisodesController'],
function(can, newEpisodesController) {

	var config = new can.Map({
		podcasts: [{
				podcastTitle: "NoiseSeurityBit",
				channelTitle: "noisebit",
				model: "podster",
			}, {
				podcastTitle: "Радио-Т",
				channelTitle: "radio-t",
				model: "podster",
			}, {
				podcastTitle: "Кактус",
				channelTitle: "kaktus",
				model: "podster",
			}, {
				podcastTitle: "MadnessyMorning",
				channelTitle: "madnessy-morning",
				model: "podster",
			}, {
				podcastTitle: "Витая пара",
				channelTitle: "tpair",
				model: "podster",
			}, {
				podcastTitle: "ПолКино",
				channelTitle: "polkino",
				model: "podster",
			}, {
				podcastTitle: "DevZen",
				model: "devzen",
			}, {
				podcastTitle: "Большой Хай-Тек",
				brand_id: 104,
				model: "radiovesti",
			}, {
				podcastTitle: "Gravity Falls",
				film_id: 591929,
				model: "kinopoisk",
			}, {
				podcastTitle: "Темная материя",
				film_id: 861614,
				model: "kinopoisk",
			}]
	})

	var globalController = can.Control.extend({
		defaults: {
			view: "templates/main.mustache",
		}
	},{
		init: function(element, options) {
			console.log("Init globalController");
			var self = this;
			self.element.html(can.view(options.view));

			self.newEpisodes = new newEpisodesController.controller("#newEpisodesController", config);
			// "#channelEpisodes"
		},
	});

	new globalController("#out");
});