define('js/newEpisodesController', 
	['can', 'js/modelController'], 
	function(can, modelController) {

	var newEpisodeController = can.Control.extend({
		defaults: {
			view: "templates/newEpisodes.mustache"
		}
	}, {
		init: function(element, options) {
			var self = this;
			self.controllerOptions = new can.Map({
				newEpisodes: [],
			});

			// разбиение списка подкастов на colCount колонок для отображения на экране в соответствующем виде
			// структура newEpisodes:
			// 	[{
			// 		data: [{
			// 			podcastTitle
			// 			episodeTitle
			// 			episodeDate
			// 			episodeDescription
			// 			showDescription
			// 		}...]
			// 	}...]
			var colCount = 3;
			var itemCount = Math.ceil(options.podcasts.length/colCount);
			for (var i=0; i<colCount; i++) {
				self.controllerOptions.attr("newEpisodes").push({
					data: [],
				});

				options.podcasts.slice(i*itemCount, (i+1)*itemCount).forEach(function(element, index) {
					var model = modelController.getModel(element.model);
					if (model) {
						console.log(index);
						var t = model.channel(element);
						self.controllerOptions.attr("newEpisodes")[i].attr("data").push(t.getLastEpisode());
						self.controllerOptions.attr("newEpisodes")[i].attr("data")
							[index].attr("showDescription", false);
					}
				});
			}
			console.log(self.controllerOptions.attr("newEpisodes"));

			self.element.html(can.view(options.view, self.controllerOptions));
		}, 
		findIndex: function(title) {
			// поиск выделенного на экране подкаста в структуре newEpisodes
			var index = undefined;
			this.controllerOptions.attr("newEpisodes").forEach(function(element, ind1) {
				element.data.forEach(function(el, ind2) {
					if (index === undefined  &&  el.episodeTitle === title) {
						index = {
							i: ind1,
							j: ind2,
						};
					};
				});
			});
			return index;
		},
		".podcastEpisodeContainer mouseenter": function(el, event) {
			// отображение описания по наведению мыши
			var index = this.findIndex($(el).data().data.episodeTitle);
			if (index !== undefined) {
				this.controllerOptions.attr("newEpisodes")[index.i].attr("data")[index.j].attr("showDescription", true);
			} else {
				console.log("???");
			}
		},
		".podcastEpisodeContainer mouseleave": function(el, event) {
			// скрытие описания
			var index = this.findIndex($(el).data().data.episodeTitle);
			if (index !== undefined) {
				this.controllerOptions.attr("newEpisodes")[index.i].attr("data")[index.j].attr("showDescription", false);
			} else {
				console.log("???");
			}
		}
	});

	return {
		controller: newEpisodeController,
	};
});
