require.config({
	paths: {
		'models': 'models/',
		'can': 'bower_components/canjs/can.jquery',
		'jquery': 'bower_components/jquery/dist/jquery',
		'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap',
	},
	shim: {
		'bootstrap': {
			deps: ['jquery'],
			exports: "$"
		},
		'can': {
			deps: ['jquery'],
			exports: 'can'
		},

	}
});
