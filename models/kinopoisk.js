define('models/kinopoisk', 
	['can', 'js/request', 'js/utils'], 
	function(can, request, utils) {

	var podster = function(initOptions) {
		return new can.Map({
			options: new can.Map({
				podcastTitle: initOptions.podcastTitle,
				model: {
					channelUrl: ["http://www.kinopoisk.ru/film/", initOptions.film_id, "/episodes"].join(""),
				},
				dateModifier: function(date) {
					var d = date.split(" ");
					var result = [];
					if (d.length == 2) {
						result.push(d[2]);
						result.push(utils.getMonthAsNumber(d[1]));
						result.push(utils.fillZeros(d[0], 2));
						return result.join("-");
					} else {
						return date;
					}
				},
			}),
			getLastEpisode: function() {
				var self = this;

				var result = new can.Map({
					podcastTitle: self.options.podcastTitle,
					episodeTitle: undefined,
					episodeDate: undefined,
					episodeDescription: undefined,
					downloadLink: undefined,
				});

				request.get(self.options.model.channelUrl, function(data) {
					data = data.replace(new RegExp(String.fromCharCode(10), "g"), "");
					var t = data.match(/Сезон \d*/g);
					t = t[t.length - 1];
					var start = data.search(new RegExp(t));
					start = start + data.slice(start).search(/<tr>/);
					var end = data.slice(start).search(/<\/table>/);
					var d = data.slice(start, start+end);
	
					d = $(d);
					var title = [$($(d[d.length-1]).find("span")[0]).text(), 
						$($(d[d.length-1]).find("h1")[0]).text()].join(": ");
					result.attr("episodeTitle", title);

					console.log({t: d});
					var date = utils.fixSpace($($(d[d.length-1]).find("td")[1]).text());
					console.log(date);
					result.attr("episodeDate", self.options.dateModifier(date));

				});

				return result;
			},
		});
	};

	return {
		channel: podster,
	};
});
