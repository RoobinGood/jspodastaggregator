define('models/podster', 
	['can', 'js/request', 'js/utils'], 
	function(can, request, utils) {

	var podster = function(initOptions) {
		return new can.Map({
			options: new can.Map({
				podcastTitle: initOptions.podcastTitle,
				model: {
					channelUrl: ["http://", initOptions.channelTitle, ".podster.fm"].join(""),
					episodeUrl: function(id) {
						return ["http://", initOptions.channelTitle, ".podster.fm/", id].join("");
					},
				},
				dateModifier: function(date) {
					var d = date.split(" ");
					var result = [d[2]];
					result.push(utils.getMonthAsNumber(d[1]));
					result.push(utils.fillZeros(d[0], 2));
					return result.join("-");
				},
			}),
			getLastEpisode: function() {
				var self = this;

				var result = new can.Map({
					podcastTitle: self.options.podcastTitle,
					episodeTitle: undefined,
					episodeDate: undefined,
					episodeDescription: undefined,
					downloadLink: undefined,
				});

				request.get(self.options.model.channelUrl, function(data) {
					data = utils.deleteImages($(data));
					var url = $(data).find("article").find("a").attr("href");

					request.get(url, function(data) {
						data = utils.deleteImages($(data));

						title = utils.fixDash(data.find("h1").text());
						result.attr("episodeTitle", title);

						date = data.find(".podcast_content_issue_name").text();
						result.attr("episodeDate", self.options.dateModifier(date));

						result.attr("downloadLink", [url, "/download/audio.mp3?download=yes&media=file"].join(""))

						description = data.find(".podcast_content").find(".podcast_content_text").text();
						result.attr("episodeDescription", description);
					});
				});

				return result;
			},
		});
	};

	return {
		channel: podster,
	};
});
