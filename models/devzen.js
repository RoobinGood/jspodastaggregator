define('models/devzen', 
	['can', 'js/request', 'js/utils'], 
	function(can, request, utils) {

	var devzen = function(initOptions) {
		return new can.Map({
			options: new can.Map({
				podcastTitle: initOptions.podcastTitle,
				model: {
					channelUrl: "http://devzen.ru/",
				},
			}),
			getLastEpisode: function() {
				var self = this;

				var result = new can.Map({
					podcastTitle: self.options.podcastTitle,
					episodeTitle: undefined,
					episodeDate: undefined,
					downloadLink: undefined,
				});

				request.get(self.options.model.channelUrl, function(data) {
					data = utils.deleteImages($(data));
					data = data.find("article");
					var url = undefined;
					for (var i=0; i<2; i++) {
						var t = $(data[i]).find("a").attr("href");
						if (t && t.indexOf("episode") !== -1) {
							url = t;
						}
					}

					request.get(url, function(data) {
						data = utils.deleteImages($(data));

						title = utils.fixDash(data.find("h1").text());
						result.attr("episodeTitle", title);

						date = data.find(".entry-date").attr("datetime")
							.replace(/T.*/, "");
						result.attr("episodeDate", date);

						link = data.find(".powerpress_links_mp3").find("a").attr("href");
						result.attr("downloadLink", link);

						description = data.find(".entry-content").find("p").text().split("Шоу нотес")[0];
						result.attr("episodeDescription", description);
					});
				});

				return result;
			},
		});
	};

	return {
		channel: devzen,
	};
});
