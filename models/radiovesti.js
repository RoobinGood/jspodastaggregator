define('models/radiovesti', 
	['can', 'js/request', 'js/utils'], 
	function(can, request, utils) {

	var radiovesti = function(initOptions) {
		return new can.Map({
			options: new can.Map({
				sourceUrl: "http://radiovesti.ru",
				podcastTitle: initOptions.podcastTitle,
				model: {
					channelUrl: ["http://radiovesti.ru/brand/show/brand_id", initOptions.brand_id].join("/"),
				},
				dateModifier: function(date) {
					var d = date.split(" ");
					var result = [d[2]];
					result.push(utils.getMonthAsNumber(d[1].replace(",", "")));
					result.push(utils.fillZeros(d[0], 2));
					return result.join("-");
				},
			}),
			getLastEpisode: function() {
				var self = this;

				var result = new can.Map({
					podcastTitle: self.options.podcastTitle,
					episodeTitle: undefined,
					episodeDate: undefined,
					downloadLink: undefined,
				});

				request.get(self.options.model.channelUrl, function(data) {
					data = utils.deleteImages($(data));
					var url = $(data).find("#brand_episodes").find("li a").attr("href");

					request.get([self.options.sourceUrl, url].join(""), 
						function(data) {
						data = utils.deleteImages($(data));

						title = data.find("h1").text();
						result.attr("episodeTitle", title);

						date = data.find(".current_date").text();
						result.attr("episodeDate", self.options.dateModifier(date));

						link = data.find(".materials_kstati a").attr("href");
						result.attr("downloadLink", [self.options.sourceUrl, link].join(""));

						description = data.find(".wrap_content_matrl").find("em").text();
						result.attr("episodeDescription", description);
					});
				});

				return result;
			},
		});
	};

	return {
		channel: radiovesti,
	};
});